<div>
   <h1 align="center">CPF validator</h1>
   <p align="center">A simple package with the sole responsibility of validating a CPF</p>
</div>

<p align="center">
 <a href="#status">Status</a> •
 <a href="#features">Features</a> • 
 <a href="#npm-link">NPM link</a> • 
 <a href="#prerequisites">Prerequisites</a> • 
 <a href="#run-project">Run project</a> • 
 <a href="#technologies">Technologies</a>
</p>

#### Status
✅ Ready-to-use package    
✅ Published in npm repository

#### Features
✅ CPF validator

#### NPM link
[CPF-validator]()

#### Prerequisites
To run the project it is necessary to have installed on your machine:
- [NodeJS](https://nodejs.org/en/)

#### Run project
1. Run `npm i` command to install dependencies
2. Run `npx jest`(or `npm test`) command to run the tests

#### Technologies
The following tools were used in building the project:
- [NodeJS](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
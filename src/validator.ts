export function cpfIsValid(cpf: string): boolean {
   try {
      cpf = cpf.normalize('NFD').replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '');
      if (exports.containsElevenDigits(cpf) && !exports.allDigitsIsSame(cpf) &&  exports.validateFristDigit(cpf) && exports.validateSecondDigit(cpf)) {
         return true
      }
      return false
   } catch (e) {
      return e.message
   }
}

export function validateFristDigit(cpf: string): boolean {
   const validateDigit = Number(cpf.substring(9, 10))
   cpf = cpf.substring(0, 9)
   const digit = cpf.split('')
      .reverse()
      .reduce((previusValue: number, currentValue: string, index: number): number => (
         previusValue + (Number(currentValue) * (index + 2))
      ), 0) * 10 % 11

   return digit === validateDigit ? true : false
}

export function validateSecondDigit(cpf: string): boolean {
   const validateDigit = Number(cpf.substring(10, 11))
   cpf = cpf.substring(0, 10)
   const digit = cpf.split('')
      .reverse()
      .reduce((previusValue: number, currentValue: string, index: number): number => (
         previusValue + (Number(currentValue) * (index + 2))
      ), 0) * 10 % 11

   return digit === validateDigit ? true : false
}

export function allDigitsIsSame(cpf: string): boolean {
   const cpfFiltered = cpf.split('')
      .filter((value, index, arr) => {
         return arr.indexOf(value) == index
      })

   return cpfFiltered.length === 1 ? true : false
}

export function containsElevenDigits(cpf: string): boolean {
   return cpf.length === 11 ? true : false
}

import * as validator from '../src/validator'

let cpf = '529.982.247-25'

beforeAll(() => {
  cpf = cpf.normalize('NFD').replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '')
})
describe('CPF validator', () => {
  describe('Unitaries', () => {
    test('Contains eleven digits', () => {
      expect(validator.containsElevenDigits(cpf)).toBeTruthy()
    })
    test('All digits is same', () => {
      expect(validator.allDigitsIsSame(cpf)).toBeFalsy()
    }),
      test('Frist digit', () => {
        expect(validator.validateFristDigit(cpf)).toBeTruthy()
      }),
      test('Second digit', () => {
        expect(validator.validateSecondDigit(cpf)).toBeTruthy()
      }),
      test('CPF is valid', () => {
        let validateFristDigit: jest.SpyInstance = jest.spyOn(validator, 'validateFristDigit').mockReturnValue(true)
        let validateSecondDigit: jest.SpyInstance = jest.spyOn(validator, 'validateSecondDigit').mockReturnValue(true)
        let containsElevenDigits: jest.SpyInstance = jest.spyOn(validator, 'containsElevenDigits').mockReturnValue(true)
        let allDigitsIsSame: jest.SpyInstance = jest.spyOn(validator, 'allDigitsIsSame').mockReturnValue(false)

        expect(validator.cpfIsValid(cpf)).toBeTruthy()

        validateFristDigit.mockRestore()
      })
  })

  describe('Integration', () => {
    test('CPF is valid', () => {
      expect(validator.cpfIsValid(cpf)).toBeTruthy()
    })
  })
})

